
# Austria repo metadata

This is a collection of apps released, paid, or required by
governments in Austria.  This is only a conduit for getting apps from
the creators to the users.  The copyrights on the files are retained
by the creators.
